FROM centos:8


# podman build --force-rm . -t test01

MAINTAINER "Björn Dieding" <bjoern@xrow.de>

ENV APP_ROOT=/var/lib/varnish
ENV PATH=${APP_ROOT}/bin:${PATH}
ENV HOME=${APP_ROOT}

ENV BACKENDS=localhost:8080
ENV USER_HASH=ecaea5a638cb64ce41e9266e550963228d0bb58ed86ca7278f1b3e135c155669
ENV INVALIDATORS="127.0.0.1 localhost 192.168.0.0/16 172.16.0.0/12 10.0.0.0/8"
ENV container=docker

RUN yum -y update && \
    yum -y install epel-release && \
    yum -y module enable varnish:6 && \
    yum -y install varnish varnish-modules ansible && \
    yum clean all

ADD bin/uid_entrypoint /bin/uid_entrypoint
COPY . ${APP_ROOT}

# Broken DNS mod 
# RUN /bin/bash ${APP_ROOT}/bin/vmod_dns.sh

RUN /usr/bin/ansible-playbook ${APP_ROOT}/provision/setup.yml -c local

RUN chgrp -R 0 /etc/varnish \
  && chmod -R g+rwX /etc/varnish

WORKDIR ${APP_ROOT}
EXPOSE 6081 6082

RUN chmod -R 755 /bin/uid_entrypoint && \
    chmod -R 755 ${APP_ROOT} && \
    chmod -R 755 ${APP_ROOT}/bin && \
    chgrp -R 0 ${APP_ROOT} && \
    chmod -R g=u ${APP_ROOT} /etc/passwd

ENTRYPOINT [ "/bin/uid_entrypoint" ]

CMD ["/usr/sbin/varnishd", "-P", "/tmp/varnish.pid", "-f", "/etc/varnish/default.vcl", "-a", ":6081", "-T", ":6082", "-S", "/etc/varnish/secret", "-s", "default=malloc,2000M", "-F"]

USER 1001